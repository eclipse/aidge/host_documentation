{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Post Training Quantization with AIDGE\n",
    "\n",
    "#### What is Network Quantization ?\n",
    "\n",
    "Deploying large Neural Network architectures on embedded targets can be a difficult task as they often require billions of floating operations per inference.\n",
    "\n",
    "To address this problem, several techniques have been developed over the past decades in order to reduce the computational load and energy consumption of those inferences. Those techniques include Pruning, Compression, Quantization and Distillation.\n",
    "\n",
    "In particular, Post Training Quantization (PTQ) consists in taking an already trained network, and replacing the costly floating-point MADD by their integer counterparts. The use of Bytes instead of Floats also leads to a smaller memory bandwidth.\n",
    "\n",
    "While this process can seem trivial, the naive approach consisting only in rounding the parameters and activations doesn't work in practice. Instead, we want to normalize the network in order to optimize the ranges of parameters and values propagated inside the network, before applying quantization.\n",
    "\n",
    "#### The Quantization Pipeline\n",
    "\n",
    "The PTQ algorithm consists in a 3 steps pipeline: \n",
    "\n",
    "- First we optimize the parameter ranges by propagating the scaling coefficients in the network. \n",
    "- Secondly, we compute the activation values over an input dataset, and insert the scaling nodes. \n",
    "- Finally, we quantize the network by reconfiguring the scaling nodes according to the desired precision.\n",
    "\n",
    "![alt text](./static/ptq_diagram.png)\n",
    "\n",
    "\n",
    "#### Doing the PTQ with AIDGE\n",
    "\n",
    "This notebook shows how to perform PTQ of a Convolutional Network, trained on the MNIST dataset.\n",
    "\n",
    "The tutorial is constructed as follows :\n",
    "\n",
    "- Setup of the AIDGE environment\n",
    "- Loading of the model and example inferences\n",
    "- Evaluation of the trained model accuracy\n",
    "- Post Training Quantization and test inferences\n",
    "- Evaluation of the quantized model accuracy\n",
    "\n",
    "As we will observe in this notebook, we get zero degradation of the accuracy for a 8-bits PTQ. \n",
    "\n",
    "\n",
    "Let's begin !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### (if needed) Download the model\n",
    "\n",
    "If you don't have git-lfs, you can download the model and data using this piece of code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:30.966145Z",
     "iopub.status.busy": "2025-02-08T23:24:30.965514Z",
     "iopub.status.idle": "2025-02-08T23:24:31.992427Z",
     "shell.execute_reply": "2025-02-08T23:24:31.990575Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File downloaded successfully.\n",
      "File downloaded successfully.\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File downloaded successfully.\n"
     ]
    }
   ],
   "source": [
    "import os\n",
    "import requests\n",
    "\n",
    "def download_material(path: str) -> None:\n",
    "    if not os.path.isfile(path):\n",
    "        response = requests.get(\"https://gitlab.eclipse.org/eclipse/aidge/aidge/-/raw/dev/examples/tutorials/PTQ_tutorial/\"+path+\"?ref_type=heads\")\n",
    "        if response.status_code == 200:\n",
    "            with open(path, 'wb') as f:\n",
    "                f.write(response.content)\n",
    "            print(\"File downloaded successfully.\")\n",
    "        else:\n",
    "            print(\"Failed to download file. Status code:\", response.status_code)\n",
    "\n",
    "# Download onnx model file\n",
    "download_material(\"ConvNet.onnx\")\n",
    "# Download data sample\n",
    "download_material(\"mnist_samples.npy.gz\")\n",
    "# Download data label\n",
    "download_material(\"mnist_labels.npy.gz\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "#### Environment setup ...\n",
    "\n",
    "We need numpy for manipulating the inputs, matplotlib for visualization purposes, and gzip to uncompress the numpy dataset.\n",
    "\n",
    "Then we want to import the aidge modules :\n",
    "\n",
    "- the core module contains everything we need to manipulate the graph.\n",
    "- the backend module allows us to perform inferences using the CPU.\n",
    "- the onnx module allows us to load the pretrained model (stored in an onnx file).\n",
    "- the quantization module encaplsulate the Post Training Quantization algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:31.998826Z",
     "iopub.status.busy": "2025-02-08T23:24:31.998172Z",
     "iopub.status.idle": "2025-02-08T23:24:32.668901Z",
     "shell.execute_reply": "2025-02-08T23:24:32.667465Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Available backends :  {'cpu'}\n"
     ]
    }
   ],
   "source": [
    "import gzip\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import aidge_core\n",
    "import aidge_onnx\n",
    "import aidge_backend_cpu\n",
    "import aidge_quantization\n",
    "\n",
    "print(\" Available backends : \", aidge_core.Tensor.get_available_backends())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, let's define the configurations of this script ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:32.674354Z",
     "iopub.status.busy": "2025-02-08T23:24:32.673894Z",
     "iopub.status.idle": "2025-02-08T23:24:32.679856Z",
     "shell.execute_reply": "2025-02-08T23:24:32.678681Z"
    }
   },
   "outputs": [],
   "source": [
    "NB_SAMPLES  = 100\n",
    "NB_BITS     = 8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's load and visualize some samples ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:32.685541Z",
     "iopub.status.busy": "2025-02-08T23:24:32.684676Z",
     "iopub.status.idle": "2025-02-08T23:24:32.726939Z",
     "shell.execute_reply": "2025-02-08T23:24:32.725611Z"
    }
   },
   "outputs": [],
   "source": [
    "samples = np.load(gzip.GzipFile('mnist_samples.npy.gz', \"r\"))\n",
    "labels  = np.load(gzip.GzipFile('mnist_labels.npy.gz',  \"r\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:32.731906Z",
     "iopub.status.busy": "2025-02-08T23:24:32.731131Z",
     "iopub.status.idle": "2025-02-08T23:24:33.115210Z",
     "shell.execute_reply": "2025-02-08T23:24:33.114039Z"
    }
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAnYAAABDCAYAAAARfEjOAAAAOnRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjEwLjAsIGh0dHBzOi8vbWF0cGxvdGxpYi5vcmcvlHJYcgAAAAlwSFlzAAAPYQAAD2EBqD+naQAANk9JREFUeJztnXls2+d5xz+8SZEUSfEQddG6JduSLfmKlDh24zmx06QNnKANmqLtWnTFNnRYW2DYgQEbsP+KdtiwtduwdEWTbm2abki6NI3rxHN8xXYk675viTqok6QokRSv/WH83liJk1q+REq/DxCgpST6fX/H+z7vc3wfRSqVSiEjIyMjIyMjI5PxKDd7ADIyMjIyMjIyMvcG2bCTkZGRkZGRkdkiyIadjIyMjIyMjMwWQTbsZGRkZGRkZGS2CLJhJyMjIyMjIyOzRZANOxkZGRkZGRmZLYJs2MnIyMjIyMjIbBFkw05GRkZGRkZGZosgG3YyMjIyMjIyMlsE9e3+okKhuJ/juO/cTYON7Tr37Tpv2L5z367zhu079+06b9i+c9+u84btMXfZYycjIyMjIyMjs0WQDTsZGRkZGRkZmS3CbYdiZe4OlUqFQqEQbmCFQiE+A0gmk+K/VCpFIpHYzOHKyMjIyMjIZCCyYfcAUKlUnDx5EofDgUajwWaz4XQ62bt3Lw6HA4CpqSkuX77M5OQks7OzXL58mWAwuMkjl5GRkZGRkckkZMPuPmO327Hb7ezdu5fc3Fw0Gg1Wq/Ujhl1ubi6RSAS73c7k5CQdHR2srq4Sj8c3eQYPBrVajUajoaSkBJVKxezsLCsrK4RCoc0e2j1DqVRit9sxGo3YbDaSySTRaJSFhQUCgQCxWOyukoJlZGRk0hWlUolSqUSj0az7PJVKEYvF0Ol0WCwWYrEY8XictbU1EokE0Wh0k0acuciG3X3m05/+NE8++SSPPfYYNpsNQIRklUql2Mhzc3P57Gc/y9zcHLOzs7S0tBCPx5mdnd0Wm73NZsPtdvPjH/8Yk8nED3/4Q5qamrh8+fJmD+2eoFar0el0PPfcc+zfv59nnnmGtbU1hoeHeemll3jrrbfw+XzEYrHNHqqMjIzMPcdoNGIwGHC5XOsqUxOJBDMzM+zYsYNPf/rT+Hw+5ufn8Xq9+P1+hoaGtsUeeC+RDbv7RG5uLvv27aOhoYGdO3diMplQq9UoFAoikQirq6vMzs4SiUQwmUyYTCZyc3Mxm80kk0kqKiqIRCLMzc1ti4fabDbjcrkwGo1oNBoikciW8lZqNBpMJhOVlZXU1tZiMplIJBJ4PB6Ki4spKSnB7/dvCcPOZrPhcDh4+OGH6e3t5erVq7f9t2azGZvNRigUIhqNsrq6uiWff7vdjs1m4/DhwyiVSlpaWvD5fExPT2fUfDUaDTqdjsbGRrKyspifn2d+fp6pqSnC4fAdvcMajYasrCxycnJYXFxkeXmZZDJ5H0Yvcz/JysrC5XLhcDiwWq0UFRWRnZ1NXl7eOsMuHo8zOTmJ0+nk4MGDLC8vs7q6it/vZ3R0lH/5l3/ZUnvBg0A27O4THo+Hr3zlK9TX11NWViY+TyaTrKys4PP5aGlpYXFxkYKCAgoLC8nNzcVoNKLVatmzZw/xeJympqZtsahZrVby8/PRaDTE43ECgQDhcHizh3XP0Ol0WK1Wampq2LdvH3BjAyssLKSiooLx8XF6enoyPvSsVCrJzc2ltraWv/qrv+KnP/3phgw7m81GdXW1OK2Hw+GMMnRul7y8PKqrq/nrv/5rNBoN//AP/0BTUxM+ny+jCqf0ej1Wq5UXXngBp9NJZ2cnbW1tRCIRZmdnN7whKxQKDAYDDoeDXbt20dPTw8rKyrZYA7cSCoWC7Oxsdu/eTW1tLeXl5dTV1eFwOCgoKECp/ECQIxaLMT4+jlarxel0otFoUKlUADQ1NfHiiy/Kht0GkQ27e4xKpaKoqIhdu3bR0NCAzWYjlUrh8/kYGxvjpZdewu/3EwgEmJ+fFx67AwcO4HA4cDgcGAwGsrKyyMrK2uzpPDDsdjvFxcUALC4ucvXqVRYWFjZ3UPcApVKJyWSisbGRp59+mpKSkowXyPwklEol1dXVVFdXo9PpxAJ9u7jdbg4dOsRDDz3E8vIyL774YsYbuzejVqsxGAwcO3aMp556CqfTiVqt5mtf+xp6vZ5r166RSqXS3pBRqVTo9Xq+8IUv8MQTT1BXV0cikWBhYYGuri5mZ2c3nBul0+mw2+382Z/9GXl5eeTm5vLSSy+xurrK3NxcWnuzFQoFer2eXbt2cfDgQZxOJ7FYjH/9138lGAym/f28W7KyssjOzubAgQN4PB4aGxuxWCw4HA6ys7MxGo1YLBa0Wu1H1j+VSoXb7Rb5dzcbfTJ3xj037LRaLWq1Gr1eDyAe6FQqRTQaJZFICEmPrfiwK5VKUfWal5cH3DiRTE5O0tvby5kzZwgGg6yurhKNRkkmk1gsFlwuF6urq8RiMQwGg5A+2epIsi9Sjl0sFiMQCDAzM0MkEtns4d0VBoMBvV6Pw+GgoqKCgwcPijzLm5Hy74xGoyiYSSQSGeW5kVAoFLjdbtxu9zo5n9tFr9eTk5OD0+kkHA6j1Wrv00g3B7VajclkoqSkhD179mAwGFCpVJSWluJwODLC6Je8arm5udTV1fGpT30KpVLJ0tIS0WiUcDh8R952k8mEy+Xi0UcfxeFwoFQqycnJQaPRpPV10el04j2vqqqioaGBgoICwuEwP/nJTwiFQlt+LddqtWRnZ7Nr1y5qa2s5efIkJpNJFErc6v5JNkAymRTG3Nra2rrfiUajGeOxl4pDpD1NsoWkw20ikSASiZBIJIjH48IOuh/cc8Nuz549lJSUcPLkSZRKJX6/n3g8TjQa5d1332V+fl54rBYXF+/1P7/pSIuR2WxGoVCIYoi/+7u/o6enh/HxcXFDU6kURqOR5557joMHD1JaWopWqyUej9Pa2kpra+uWXxCMRiOFhYU89thjPPPMM7z33nu0tbVlpFFzM2q1mhMnTlBaWkpDQwNlZWVUV1ejVn/0lSssLGT//v3ieRkdHWVqaoqxsbFNGPmdo1Qq0Wq11NXVUVNTc0cn78XFRXp6eiguLhY5l6FQ6CMLfqZisVior6+npKQEp9Mp1sgLFy7Q1dWV9huZQqHAYrGwb98+/uiP/og9e/ZgNBp56623aGtr4wc/+MEde1gff/xx9u/fT0lJCePj47z88sucO3cOr9ebtuuBSqXikUceobq6mi996Us4nU5cLhcqlQqfz4fFYmF5eXlLeZ1vRVZWFrm5uRw/fpzGxkb0ev1tGeM+n49AIMDY2Ngt7/Hg4GDG7IEOhwOTySTyCPft20dJSQl5eXkkk0kWFhY4ffo0k5OTTExM4PP5WF1dvS9jueeGXXl5OXv37mXnzp2oVCqWl5dJJBLCjX6zUTc3N7fh719bWyMajTI+Pk4kEmFlZeVeT+GuSCaTzM/PMzg4yJkzZ1hcXGRhYYHh4eFb5pyo1WrKysooLCxEq9WuO7lslc3sk5B0/SwWCyaTSTz06by53Q5KpZLa2lqqq6upqKjA4XCs8z6lUimx8OXk5IgQrd/vx+1209nZid/vZ2VlJWPyS0wmEzk5ObjdbrKzs5mYmGBpaWnD36NQKDCZTFgslnX5NpmOQqHAarVSW1uL0+kU84pGowwMDGREoZRSqcRqteJ2u6moqBAe6N7eXrq6ulhcXNywEabRaDAYDFRWVrJz504CgQBer5eenh7m5+fT1qjTaDTo9Xpqa2upqanB4/FgNpsxGo3AjUPr7t270Wq19PT0iGhVpqJQKNBoNCK8GgwGiUQiwkEj/VytVou9S7IBVldXRZRqeXlZeHbn5uYIhUJMTk7e8j7PzMyk7TVTKpUixOxwOCgtLSUnJwer1YrdbqeyspK8vDzsdjupVAqXy8Xy8jJzc3P4fD6mpqZYXl4mEAjg9/sZGxu7Z3O9p4adQqHg2LFjPPHEE+Tn539kQX7yySdJJBKsrq6ysLDAzMzMhv+N+fl55ubmePnll/F6vQwNDd2r4d8TYrEYbW1tDAwMcP78eaLRKNFolGAweMsHV6fTcejQIUpLS7fMBrYRDAYD+fn5WCwW1Go17e3ttLW1pe3LfLuo1Wo++9nPUl9fv24ukkF382m2uLiYHTt20NjYyNraGjMzM/z6179mYWGBoaEhAoHAZkxhw+Tl5VFRUUF1dTUmk4m33nqLgYGBDX2HVD3sdrux2WwYDAY0Gs2WKKTRaDQUFRXx3HPPsWPHDvH58vIyFy5cYHBwcBNHd3uo1Wo8Hg8VFRXU1NQAEA6HOXPmDC0tLXdkhJnNZvLy8jh69CgNDQ2cO3eOK1eucPHixbROx8jKysLpdPLcc89RU1ODxWL5yM+/+MUvcvXqVbxeLysrKxmtyabRaDCbzTQ2NrJnzx7a29uZmpri6tWrpFIp4vG4cLZEIhGUSiV6vZ6BgQFGR0cZHh5mYWGB7u5ufD4fXq+XtbU14vF4RhZJ6XQ6PB4PdXV1HD58mMbGRgoKCrBarR8brThw4ADJZJJEIsHU1BQLCwu0tbXR3NzMj370IxGivVvuqWGXSqU4f/48S0tLFBcXk0gkWFpaEsmTLpdLyHpkZWVRUlJCKBQikUhgNps/cjGkCyDl2uj1etxuN36/n66uLnQ6XdoZdhJra2vi9PpxJ7U9e/ZQWVkpDBtAeDJHR0czTvrgTnA6nRw/fhy73c7i4iJer5eZmZmMnvcjjzzC/v37hV7Tzc/1h8v819bWiMViJJNJzGYzGo0Gu93OkSNHcDgcfP/73+f69eubMY3bRq/XY7PZOHbsGEeOHEGlUjE2Nsbbb7+94ffT4XBQW1uL2Wy+T6PdHKQE8cLCQjweDyaTSaRjrKys0Nvby+zs7GYP8xPR6/VYLBZqa2spKSkBoLm5mY6ODrxe74bDSmq1GrvdTmNjIydOnKCkpIRYLEZ3dzdDQ0MiJztdqaqqYs+ePbhcLgwGw0d+rtVqqa2txW63k5eXR0dHB4ODg7S2tmachItGo6GyspJnn32W3bt3U1BQwPj4OD6fD4BAIMDw8DAvv/wyZ8+eZXFxEbVajcViYWpqSqRgSR6+1dVVsfdL70EmYLFYMJvN7N27l/z8fA4ePEh+fj4ejwe3243BYCAUChEKhZiZmWFlZYVYLMaBAwfQ6XTMzs4KgXrpfaquriYej9PQ0MDg4CBTU1N3Pc57HoptbW3F5/NRVlZGJBJhcnKS3Nxc7HY75eXlOJ1O9Ho92dnZWK1WYrEYa2trWCyWj3isEomEsOhNJhN2ux2AUChEeXl5WufoxeNxlpeXb/kzabOvrq6mvr4eh8Mh3PdLS0t4vV6mp6dZWFjImAf+TlAoFOTk5HDgwAGMRiPBYJC5uTkWFxczdt5KpZK9e/fy9NNPY7VaUSgUt5yL9GwHAgGi0SjxeBytVotOp8NsNovwzi9+8Yu0zznU6XS43W7279/P8ePHmZ+fZ3Jykqampg3nFlmtVsrKyjAajRnt3fgwSqUSh8OBy+USBr/k5QiHw4yNjaW1dwpuhBZzcnKoqKggPz+fZDJJT08Pb7/9NnNzcxu+X1JYb+/evZw6dQqTyUQoFGJoaEh4c9IVhULBjh07OHDgAFarFY1GIwyUVCqFUqlErVZTWlpKUVERtbW1vP3221itVoaGhlhdXc0Yw04KOZaUlHDq1Ckhy3WzMSuFWs+cOYNWq2VmZgatVovdbicQCBAKhTJ2TYcPwsxSqsmRI0coLy/nU5/6FHq9Hq1WK97nxcVFZmdn6e3tZXFxkXA4TGVlJSaTiYmJCdxuN1arFa1Wi9FopKioiEgkQl1dHYFAID0Nu8HBQcbGxkTi/9ramoi763Q6dDodDocDp9OJ2+1mdHSU1dVVqqqqPpJYHovFCIfDTE1N8ZnPfIavf/3rZGVlkUwm8fl8aW3YfRJ5eXmUl5fz/PPPc+DAAbKzs0kmkywtLXH27FnOnj3L1NRUWi9sd4tSqcTpdFJUVER1dfU6+ZdM3dAtFgtlZWVUVlZSVFSEWq2+ZeVTOBzmnXfeYXJykp6eHsLhMIlEgtraWoqLi3nqqadQq9WkUikaGhqIx+O88847abvxezwevvzlL1NbW4tKpeLFF18UXonbzQ+UZGGKioqoq6tDp9PdUQ5uuiKJ+O7evVt8lkwm6erqoqurKyM2+aeeeooDBw7wzDPPoFKpGBgY4Nq1a5w/f/6OigMMBgO1tbWUlpZis9lobm6mr6+P06dPp/W91+l02Gw2Dh48yIkTJ7BYLESjUSYnJ4WBU1lZKfIPpUrohoYGduzYwaVLlwiFQvj9/s2dyG2gUqmwWq1861vfEnp0169fp7Ozk4sXL4piQImlpSUUCoWoAJV0GTPZqAOoqKjgc5/7HLt376akpITCwkKysrIwm83Mzs4yMTHB9PQ0Pp+P//qv/2JpaYlQKEQ8HketVlNeXo5Op+PFF1+koaGBz3zmM7jdboxGI4FAgLKyMr7zne8Qi8UYGxsTtQl3yj037CKRCJFI5GO9VUqlEp/Ph81mY3p6momJCdGJ4VaGXTweFyrUcCPEKQn83klidjpgNBrJy8sjPz+f3Nxc1Go1oVCI2dlZRkdHGRgYyMicg40gycJYrVaysrIIBoPEYrGMXQSkEOqePXsoKCjAbDav80BLJe4rKyssLS3R3t6O1+ult7dXhJyUSqUIz0pe3ZKSEoLBIN3d3SwuLhIMBtPm+kgipG63m6qqKrKzs4lGowwNDTE8PEw8Hr/tsWq1WgoKCnA6nZhMJhGquVc5J5uNSqUiPz9f9IZOpVIkEglGRkYYGRlJm3v6SdjtdjGHSCTC+Pg4fr+fpaWl2zbgpfxS6buqqqrEGjg8PExbW5tIqE9XpH7fdrsdh8PB2toafr+f69evs7q6SjgcRqFQiMR5jUYjisSUSqXoKtTV1ZW2BRUfvk+SAa7RaJienqajo+OW9+nm50DqAZupSLIldrudkpIS6uvrqayspLCwUEiSzc3NMTw8TFdXF5OTk/h8Pjo6OsTaBTcOAlJIdmJigtzcXAYHB7FarZjNZrRa7boiQqlD1d3wwAWKk8mkKP/2er3Cdd3W1nbL37darTzyyCOUlJRgNpuZmJhgZGSEc+fOMT09/YBHf2/IycmhqqpK3ESAubk5zp49y9WrV2lvb0/Ll/1eotFoxOkHbnix/H5/WoccPw6VSoXL5aKxsZG//du/xWKxYDQa1+XWSQbd1atX6e/v58UXXxSdFeDGIuLz+fD5fDz//PNYrVaMRiNPPvkkDQ0NAHR0dHD69Om02Qx0Oh0PP/wwjY2NNDQ0iGrG/v5+RkdHN2Ss2O12XnjhBZFcPDw8TF9f37prlMloNBp27dolRLglj8Z///d/09ramhGVzzeHGiORCAsLCxuu2lapVBgMBp599llqamo4deqUaKP4y1/+kl//+tdpbwyYTKZ1aUUdHR20tLTw7W9/W3hojhw5QlVVFV/60pdwuVwifKnX6/n2t79Na2srf/mXf0kwGEzL51uKsJ04cYK6ujqOHDmCTqcjEAhw+fJlfvGLXxAIBDJyvb5d1Go12dnZPP/88+zfv5+nn35aaHNKWqv/+7//y9WrVzl37pxooffh90Gq8pdUL6TikYKCAoqKitDpdPdcp3HTOk98OGHyVhuVpER+7NgxKisrUSgU9PX10d7eTiAQSNvQ1MdhsVh45JFHqKur49ChQ1itVuLxOCMjI3R1dXHhwgW8Xm9abNr3G5VKhcPhEEUj4+Pj4sSbiahUKnQ6HdnZ2ete1Fgshs/no6enh6amJvr7+5mZmRHP7833OhgMMj09TVNTk5B+0Ol04rlRqVRcvnw5barrpBCDx+NBp9MxMzMjWkBtxKgzGo04HA52796N2+0mlUrR29tLS0vLR65RJmKz2cjPz6ewsFDkCUs5lrOzsxkhc/JhsrKyKC4u5vDhw+h0OlENOTQ0dMv7ZTAYsNvt2O12cnJyOHr0qJAHWV1dZXJykkAgkPZGHXyQb6VUKkkmk3R3d9PV1SU2dilMvba2xvz8vNB4k/5W+u9+CtTeLRaLhYKCAvbt20ddXR3xeJyJiQnOnj1LR0dHRuUI3ikOh0MUSFRWVqLRaFhbWyMSiXD27FmGh4c5f/48Y2NjwiN38/1UKBTodDpMJhNjY2M4nU6efvppUWMg5doqFAqWl5fxer0iV/Vur21atxSTxGufffZZka9w/fp1Ll68iN/vz7gcNIfDwe///u9TXl4uxGojkQitra1cvXqVN998My027AeBSqUiNzeXnJwcAPr7+zl37tzHhvDTHUlp3GQyrTt9RSIRhoaG+O1vf8tLL73E8vLyxz63wWAQr9fLO++8QyqVYufOnajVasxmMydOnEClUvGzn/1MCH5vNpLXtaysDK1Wy+joKO+9996GtCUlwVtJ0NNisZBIJGhpaeHdd9/dEikJubm5lJSUUFxcLEKxkjSEz+dL63yyW5FKpTCbzezatQutVsv+/ftFFeBrr712Sw+eZLhXVFRQVFTE7t27RcvEqakp2traMkbWR2p9JeWSNTc3C6kXKYG+r6+P+fl5ZmZmsFqt4hmWwpNS2km6GkcOh4M9e/bw6KOPUltby8zMDK2trXz/+99naWkp45wqd0JeXh5VVVUcPXpUvLeSp/qXv/wlnZ2d9PT03PIeSmFck8mE1Wqlv78fgK9+9asYjUaysrJEdy64oe/b1NSE1+slHA5vXcNOpVJx+PBh6uvrsdlsqFQqVlZWmJ2dZXp6OuNcwJKFXlVVhcvlEkbd3Nwcr7/+Ov39/ffkhmYCBQUFeDweHnvsMRwOB8PDw3R3d9Pe3p6WYYmNInmjp6amGBoa4oc//CGDg4MsLy//To/E6uoqTU1NOJ1Oamtryc/PFxXT6YTknSwvLyc/Px+A7u5uTp8+TTAYvO3vUalUNDY2UldXJ6rkpfZ7PT09GeHB+V3k5+dTXFy8Loe4p6eHjo6ODV2rzebatWuEQiHq6+txOp0iV9hms4nDRk1NzS0NcanllBSO1Ol0ooLw2rVr/OIXv8Dr9W7CrDaO3W7n0UcfFc3sd+3aRSQS4eLFi+vWb7VaTXFxsWgtCTciU2NjY4yMjIj2gelITk6O0KNMJBKMj48zMTHBwsJCxjlU7pRbeVal/+3xeNBqtRw8eFAYb/CB4W4wGESRqCRtYjabxd6vUqmExzcUCtHf38+rr75Kf3//PbFt0tKwkxpMV1RUUFFRIZIP5+fnWVhYyLimygqFAofDQV5enmg7olQqhVBzf38/ExMTGWes3ik2m428vDw8Hg8ajYapqSnm5uaYm5tL24VuI0iLwfj4OL29vVy/fp2lpaXbWhCl0O38/DyBQGCdNIaU85EOng0p9JyTk4PRaCQcDjM7O8v4+PiGvkehUFBYWChyTaRcy4WFhYwtjvowkjK9UqlcZ/R3d3dnlOdjcnIStVrN2NgYa2troo+rVCWqVCopKCgQBUA3I4WeTSaT8NRJ99rr9dLZ2Zkx3nqtVovNZhMpF7m5ueTm5orwrEqlEsnw0vtxs7ErFQWmc/9bo9FIfn4+er1e9HmPx+NC1kWhUGyJtfqTkJ7jSCTC2toaOp0OuLH2FRQUYDKZcDgcWK1WnE6neLfX1tbEocfpdGKxWAgGgygUCvHswwfdpSYnJxkZGaG7u/ueSZylpWEneXQ+//nPCxmUCxcu8KMf/Yjm5mZmZmYyygjSarV861vfor6+HrvdLqolm5ubaWtrY2RkJC026wdFRUUF9fX15OTksLy8zNjY2G0bPumOdMqLxWL8zd/8zR2JkUrfITWUvrlC9hvf+AavvPIK586du3+TuA2k5GqHw0E0GhXSBxtFoVCI5HIpKXloaCjtWgXeDUVFRVRUVKBWq0WV//vvv89rr72WEZIXEl6vF5/Pxx/8wR+Qk5PDjh07qKiooLi4mFOnTmG1WgkEAvT399Pa2rrub6emprh8+TLf/OY3OXXqFHDDsOvo6KC/vz+jcot9Ph+//e1vcTqdlJWVUVpaSjgcpqSkBKPRiNvtXtcnVKPRsLy8LLqoPPbYY7hcLn7zm9+kbSje4/Fw7Ngx7HY7Wq12ndZob2+v6CSRSQeTjTIwMEAwGKS5uZny8nKRPmAymfja174mDFylUim8b/DB+i155aQ2fDeTTCZpb29ncHCQH/zgB0xNTTExMZGeLcXuFTabjaKiInEqkgRPh4aGPrY1V7qSnZ0tFkGpzVo0GiUUColwzFZIEN8IkgdDCkePj49nzGn9VqjVaoqKinC5XOKzVCqF3+/f8Mat0Whwu92isESj0YjvC4fDeL3etJCCSCaTQr5Fyi+UyvZvV+ZE8sxLsj+SF9vn86VFDuHdotfrhQCpx+NBpVIJbc9gMMji4mJGeT2knLCZmRlCoZAomPD5fGi1WsxmM6FQiImJiY+0kvP7/aLpudRtY3Z2ltbWVsbHxzNqTQ+Hw0LqJZFIYLFYKCoq4vHHH8dgMJCTk0NlZSVOp5Pl5WWCwSD9/f1UVlbi8XjIysrC4XBQWVkpJDPSDa1WK9YfhUKBXq8nNzeXAwcOCIPWarXi9/tZXl4WXspoNEosFltXQKXT6YRxG4vFMubdlgTk33vvPSYmJpiYmBBRN4vFglKpFN5XKWVEkoCSNHslr+zN3llJs/X69ev09vYyNjZ2zxUh0tKwk0RKTSYT0WiUzs5Ourq66O/vz6iFEG6cfKqrq6msrKSgoACFQsHCwgIDAwO8+eabXL16dUufem5Fbm4uhYWFKJVKFhYWuHz5csZK18CNhevo0aPU1dWty8W4E7Kysti3b59o2ySdBFOpFF6vl1deeSUtNoK1tTVCoZCo9vJ4PCLN4HYPXwaDAZvNRn19veg7urS0RE9PT1oYr3dLTk4OpaWlPPTQQxw6dAilUkk4HGZ1dZVAIJCRAuupVEqI8M7OztLZ2QnAj3/840/8O6mlpPReTE1N0dHRwcsvv5xRXku4sTG3t7czPT1NOBzG5XKRl5fHQw89tO5AE41GuXbtGu+//z7//u//zje+8Q0++9nPUlhYiNvt5sknn0SlUtHd3b2Js7k1Uk6kFF6UegR7PB6RR/baa68xNjZGV1eXaJM1PT0t2otJxk5eXh5Wq5XOzk78fn/at86TkHJA//mf/xmNRoPRaOTxxx/n4MGD1NbWotfrCQQCLC8vs7S0JNISKioqcLlclJeX3/J7x8bGuHTpEj/96U/p6upieXn5nheIpZVhp1AoUKvV5OfnU11djUajYXFxkbfeekuIOWZKhZyUg1RbW8vRo0exWq0iBCuVNgeDQaLRaMbM6W6R+v1KOVV+v5/x8XHa29uZn5/f7OHdMRqNhp07d65r7L4RpFZiOTk5lJSU8Pjjj1NRUSFOeZKHQ0q2TgfvrpQsv7i4iMViobS0lCeeeAK73c6VK1duWRQgSdwYDAbMZjNWqxWbzSYkQOCjMkiZjLQZqNVqoWm4vLxMb2/vlskfvF1cLhcnT57E4/EQj8c5d+4cra2toqVeJpFIJFhZWeHChQsAnDp1CpfLJTw0qVSKrq4uxsbGePXVVxkdHWV2dpbBwUE6OjrEO1BVVUVfXx92u/0Tq+U3A0napLa2VlSE3oxarWbfvn2Ul5dTX18vjJqVlRXh6ZLWKaPRiFarxev14vf7GRgYoLOzk2vXrj3oad0xiUSC1dVVWlpamJqa4uLFi6hUKpEnF41GUSqVmM1mXnjhBZGPB4i8u6WlJc6fP09bWxtXrlxhZGSESCRyX9a7tDLspNCMy+Vix44dKJVK/H4/V69eZWxsLC02tNtFkqkoLy/nwIEDQgYjkUiwvLzM9PT0hsU9Mx3pFJibm4vL5cLv9zMzM7PhRvHphkql+kgodiNIFVM7duygqqqKgwcPkp2dDXzQoSAQCLCyspI2h5tkMkksFmNhYQG3243FYuHAgQMUFBSwtrZ2S6+iRqOhpKREPAM5OTlYLBbhGZCKTtLFeL1bNBoNJpNpXTXsysoKIyMjGVUNe7dIBv3DDz+M2+0mkUjQ2tpKS0sLKysrGXevk8kk0WiU9vZ2VlZWaGhowGg0ioN7Mpmkv7+ftrY23nzzTaHPNz4+Tl9fHw899JBIzykoKBB5qulk2M3MzHDt2jVyc3MxmUwiJAsf9DovLy+/7QKQm1NTpGYE169fT5v17HchpVAMDg4yODh4y99RqVTk5ORw8uRJoRQgzS0cDjMzM8OZM2fo6Oigqanpvj73aWXYFRYWcuLECQ4fPkxpaal4OTo6OjIumbqwsJCnn36aRx99lKqqKvR6PcvLy7S0tHDmzBleffXVjA4/3gnFxcU8/PDDlJaWYjQaef/995mYmNjsYd1zNrpQHTp0iIaGBk6ePCnaEKlUKrEYzs3N8YMf/ICenh58Pl/aHAZWVlb4+c9/Tk1NDbFYjKKiIqqqqvjTP/3Tjx2jdLKV+mdKek7SBjEyMsKbb77JwsLCg5zKfaGwsFBI+kghzJGREX71q18xPDy82cN7IOh0Og4dOsThw4c5efIkGo2G1dVV0X0oEzb1j2N8fJy5uTm+973vUVxcTGNjo+go8sorr9Dd3U0gEBDvwtWrVxkeHiY/P59du3ZRU1PDQw89RDQa5Sc/+Qk9PT2bPKMPaG9vZ2JigvPnz1NSUsIf//Efk52djVKpJDs7m6ysrA1X9ZrNZrKyskT0SqFQcObMmTsqukpHjh49yu7du3niiSeEl1OhULC2tsbLL79MW1sbb7zxBqFQ6L4fZtLCsJNabuTl5bF3717cbjcajYbx8XFGR0cJhUIZp2eVnZ0tdMgMBgMKhYJwOExXVxcDAwNMTk5mXAjibpEqxqR2W6FQKGM7TXwSt7vgSSXxVVVVVFVVUVFRIQSbpQ1vdXWVxcVFuru7hcxEuiDpW2m12nV9MrOystb1yZWIx+PMzMyIja6wsFB46eGDApH5+fmMe99vRkopsdvtlJaWkpWVJdpwBQIBxsfHt0QO4e2gUqkoLCykoKCAnJwc0VtW8kBnMlKhQE9PD36/H51OJwy74eFhpqen1z3HUj/ssbExbDYbdXV1OBwOKioqyM7ORqVSpU0RiZT6oVAo8Pv9NDc3izFKEh5KpRKtVovD4RChWMnzfisk/TatVovb7RbvRqaj1+sxmUxUVVVRW1srNB7hhrTN6uoqfX199Pb2Mj8//0A81Glh2Enl1EeOHOErX/kKyWSSYDDI66+/Tnt7e9o87BuhoKCAz3/+8+sqY3w+H//0T//E3NzclhDi3ShZWVm43W6xAIRCoW15HSRqamr48z//c/Lz87Hb7bcUIp6dnWVkZISOjo60S7ZPJBJ4vV4mJye5dOkShYWF5Obmkp+fvy7HRCIcDvN///d/oljoC1/4Ak888QTHjx/HZrOJVIVMP/Co1WqcTic7d+7k937v91Cr1SQSCRYWFvB6vR+rVr8VkTpTVFZWAoj2id3d3YyPj2e0xw4QLcV6e3t59913xee3qgyXpG6uXr1KLBbjxIkTuFwu6uvrcbvdovAoHa6JlOs6MTGB1+ulpaVF/KyyspLCwkKRD//lL39Z9D39OKPuw5/bbDaqqqowmUz3dR4PAqlA6gtf+AK1tbXrug8tLCwIqZ8H+d5vumGn0WiwWCw8+uij7N27F61Wy/T0NFNTU4yPjzMzM5MWD/pGkfoJ3vxAS7kJGo1mXTuRm0kkEutazUiCl5IHRKFQoNVqsVqtlJaWfuTvJfHT4eHhtMtfsFqtlJWVYTQaicfjjI2N4fP5NntYd83N/R+l+61UKsnPzxcVf1KYUqfTiZyr4uJiqqqqMJvNGAyGdblY0vdcuXKFK1euCJmIdONmUU5JlT4YDK6bi0QsFiMUCgmPXTAYZGlpaV3YVuopKnk3MhGNRiM6MqjVaiHmOjo6yvT09LYx6uCGkbtjxw5cLpe4Bk1NTRvuJ5zOSHmwt+OASKVSjIyMkJWVRV9fH1arlezsbCorK5mbm6OpqSmtvPLS+33zYWtmZoZIJCKiLgMDA2i1WgoLC0XF7PDwMIuLi4yPj4sDn9RT+sM6nZmKWq0mJyeHXbt28dhjj1FQUCDSSqTWct3d3Vy/fv2BSxttumGn1+ux2+089dRTeDwelEolPp+P7u5u4c7eKiiVSoxGI2trax+7qIXD4XWVslJ1rcFgEN9hNpupqKjgM5/5zLq/TaVSJJNJLl68iNfrJRqNppW30263s3v3bqF3JXXc2EpIC5VKpaK0tFTcx+PHj/P8888LbUbpd6VF4FbPQyqV4re//S1vvPHGg5vAXRAIBAgEArfdGmplZeUjC54Unl5bW8tYw06n01FcXIzdbhfdJqSQ3VbJJ7pd1Go1FRUVoqimt7eX8+fPZ7Ru5d2QTCbp6+sjFovR0tJCTU0NhYWF7N27l3g8Tnt7e1oZdrdC6hIEN975jo4OcnJyqK+vFykH169fp7u7m7fffptPfepTHDhwAIfDsc6hkekV8DqdDo/Hw4EDB3jmmWcwGAzCASNFHpqamnjttddYWFh4oHvxphp2SqWSF154gUOHDlFSUoJGo2FmZoZ3332XM2fObDlJAI/Hw/e+9z0hangrBgYGhBQK3Eg4lZTrJSQZhdzc3I/8fSqVwmKxoFAoOH/+PFNTU/dnMhtAr9dTXFyMx+PBarXe0puTyUQiEc6ePUtdXR1PPvkkcGND+/rXvy48bVLrGbVa/ZHF7FYL3OjoKD09PRmj+XQn3Eq8MxgMMjY2ltEheqn5t5RyIOUNnj59+mMr6rYi+/bto7q6GqfTiV6vJxKJ4PP5GBkZyfhw+92QSqVYXFzk9ddfJx6PU1NTw+HDh/F4PJw5c4apqam0S7v4OFZXV2lra6OsrAy4sadnZWVx9OhR0Ue6traWsrKydWHXSCTC4uJixh7e4EZq0f79+yktLcVgMAhJo7W1NXp6evj5z3/O5cuX6evre+BatZu2w0ohyZqaGhoaGrBYLKysrDA9Pc3g4CA9PT0ZLdybTCaJRCLo9XphxZvNZo4cOSJ+R0o0vTnhtKCggJGREfFiS+HL3bt33/LfkYzEm3M6XC4XBQUFwsu32Wi1WpFHptPpROm4lHyc6cTjcQYHB7Hb7SQSCdFGRhLdle7LhxOLbxYzTiaTJBIJ4vE4y8vLjIyM0NbWtq1kMeDGopjp3hzJqy55J6ROMyMjI8zMzGzy6B4cHo+HqqoqjEYjCoWClZUVgsGg6JspheNubrK+XQiHw/T19VFbW4vf7xc6eAUFBUQiEZaWljLimsRiMWZmZlhcXCQcDosuE9L+k0gkKC4uJi8vD/hANiQQCGR0hxmFQoHBYKCsrAyn0ymcFZLGodfr5cKFC4yMjGyKg2rTDDuz2SwqgsrLy1Gr1QwODvKP//iPNDc3Mzk5mVZhxI2ysLDApUuX2LVr18cK197KY1FVVUVZWdm6HDvpoZEWwJtzdKS+fYODg8LLMTExQX9/f9oouptMJo4cOUJFRQVKpRKv18vQ0BAtLS1bwiMVjUa5dOkSGo2GZ599FqPRuCGjWlLyl67LD3/4Q+bn51lcXEybe3g/yISN604wmUwcOnRIvPdSO63l5eWMPqxuBIVCQWNjI8eOHUOn0zE7O8vFixdFao1erxc6fysrK9uqVzbc8FgNDAzwzjvvoFKpeP755yksLOSrX/0q7733Hv/2b/9GLBZL+z0wGo0yOjpKa2srb7/9NocOHcLtdqNQKLBarezfv3/d/iX1Xn3rrbf42c9+lrFROb1eT0FBAV/84hexWCzi82g0ytmzZ7l8+TLNzc2bJk31wA07qe9ccXEx+/btE6KufX19dHd309/fz+LiYto/0L+LhYUFzp8/TzgcJhQKrUuslzAYDOh0Olwul/DqaTQaVCoVPp+PSCQiNoJUKsXCwgKrq6vr9J/GxsaYmZlZJ5+yuLgoElw3G6VSicFgwOPxYLPZgBuVnhMTE4TD4S3hsUsmk/j9fiYmJrh69Sq5ubnY7XYKCwvRarXi9z7srbu5WnpiYoK2tjZGRkYYHh5e12liqyJ5NuGD/NBMN/akoqmcnBxR5Sz1kry5KGo7IOUGS9XOkUgEk8mEx+OhtLQUvV5PKpVidHR02xl2cOO5mJubo7u7m/n5eVwuF6WlpUxNTWE2m1leXk77fVAqrPB6vbz//vu4XC7hsVar1Wi1WmKxmBDonZmZ4fz583R1dbG0tJRx67/kaGloaKC+vh6LxSKeY0maqrm5mb6+vk3NlXzghp1Go8HhcHD8+HG++c1v4nQ6WV1d5dVXX+X69es0NTVl/OION3Llvvvd73L8+HH27dsnquNuxuPx4HK5RPNoiWQyybVr15iamlpXFSw1Ix4ZGcmYDV+q4JVK+uGGl7G5ufm+tVN50CSTSRYWFmhububv//7v2bNnD5WVlXzuc5/D6XSu+13JoLv5WWhtbeXs2bP8z//8T1r0gX1QSI3DpXSET8o9zRSkYie3243ZbAY+MOy2SkeNO0Ey3MvKyjCbzXz+85/HbDYzOjrKW2+9RV9f32YPcVPwer0EAgHRraCuro5gMEheXp5I50lnpPe2vb2d0dFRsrKyCIVC7Ny5E6PRiMViEb1UT58+TWdnJz/+8Y8zzqCTUKvVZGdn8xd/8Rfs3bt33b49NzfH0NAQ//mf/7npag8P3LAzmUw0Njayc+dOcnJy0Gq1hMNhRkZGmJ6e3hIb/c309vayuLgoPBM3I8lcnD59ep2gq3SKXVlZWSfiOTMzkxGnuJuR8sakE5vP5+Py5ctpV9Z/L1hZWWFwcBC/309nZyfBYJDCwkJqampwuVy43W7GxsZYWFigra2NWCyGQqHg+vXr9PX1ZXTBwJ1QXFzMwYMHhYf6vffeS8uG6BtB8mBMTExgMBhwu90Eg0EWFha2rVEHkJOTI9b9UChENBplYGCAM2fOpFXHhQeNlFP6+uuvMzIywne+8x1cLhfHjx/n7NmzGdOBRZI5euONN2htbeXw4cM4HA6KiorE+tbb28vc3FxGH95cLhdFRUU4nU5xcJN47733aGpqSos9+oEadgqFAqPRyJ49eyguLl7XP3V6ejpjHuKN4PV6b1v+YSsi9RQNBAIsLi4SCoVob2+nq6srY09tH0ckEmFqaoqpqSmUSiXhcJiSkhJUKhWxWIzs7GzGxsYYGRnhjTfeIBwOo1AoRPJxpiYS3ykul4uKigoSiQQ+n4+LFy9mfN9gSdNvcnISp9NJPB4nGAyua4q+Xbg5BJ2VlSXu9draGr/5zW8YHh7mypUrGZtndS9IJBKEw2EuXbrE5OQkf/iHf4jFYuHQoUN0d3cLb3a6IxXwXblyBYPBQDweJz8/n/Lycs6cOcP777+P3+/PaKMObkh2lZSUYLFYhLdO0jBsb2/nvffeIxwOb/q7/sAMO4VCQWVlJTU1NTz77LM4nc6MFieUuT0SiQTDw8P8yZ/8ifj/s7OzW96ISSaTdHZ20t/fz5UrV0S+0crKCmtra/j9fvHyS4tipi96G8Xn89HZ2cmZM2cYGRnh0qVLGV8Rm0wm8fl8/OhHP6KxsZH5+Xl6enoYGxvbVvc3lUrxq1/9ir6+Pl544QUhWtvS0kJ/fz/T09OEQiFmZ2c33buRDvj9fqampmhpaaGwsJDDhw/T3NxMb28vU1NTGRXdiEQinDt3TrQZCwaDWyZf+OGHH+bUqVPY7XbxWV9fH9euXeP8+fN0dHSkhcPigRp2DocDt9uN2+0W/RPj8Thra2vbLrF4OxGJRLaVfpeEFErfzh6JT2JycpL29nba29vxer34fL6M8E78LtbW1piYmMBms2G32/F6vczNzW07A2ZiYoJEIsH169eFYdfa2srAwIDY6LfbNfk4pHZjnZ2dpFIpduzYgdlsxmKxbHq+1kZJpVJbtprfarWKojjJfpmdnaWjowOfz5c2/Y8fmGGnVCopLy+nqqqKrKwsNBoNcKN6dHp6mtXV1Yw6lcjIyNwdv/rVr/j1r38tDnVbwaiDG5v0/Pw87777LhcuXBAC1NvNiBkdHWVsbIz3338f+ECvUT7A35pgMMh3v/tdjh07xu7duzEYDOTn5zMyMrLt8m/TFb1ej9lsRqVSkUgkCAQCtLW18eqrrzI/P7/ZwxM80Bw7qTecQqEQMiBnzpyhu7tbuOZlZGS2B7fbXzNT2erz+13c3EdY5neTTCZZWVmht7eX//iP/6C9vZ2xsTH5+qURXq+XtrY2HnnkEQCuXbvGwMAAgUAgrULNmyJQnEwmCYVCeL1eXnnlFS5fvkwgENgyJ3YZGRkZGZmNEo1G6ezspLOzc7OHInMLBgYGOH/+PHv27CGZTPL222/T3t6edrnBitRtWlN3W+igUCgoLCzEYrGwY8cOYrEYkUhEyIHcb2v3bozGTC/yuNO5b9d5w/ad+3adN2zfuW/XecP2nft2nTfc3dztdjtWq5WSkhIAhoeH8fv9D7S37+3M/YEZdpuN/PJvnO06b9i+c9+u84btO/ftOm/YvnPfrvOG7TH32zbsZGRkZGRkZGRk0puPtkOQkZGRkZGRkZHJSGTDTkZGRkZGRkZmiyAbdjIyMjIyMjIyWwTZsJORkZGRkZGR2SLIhp2MjIyMjIyMzBZBNuxkZGRkZGRkZLYIsmEnIyMjIyMjI7NFkA07GRkZGRkZGZktgmzYycjIyMjIyMhsEf4fBkUzgMVYW6IAAAAASUVORK5CYII=",
      "text/plain": [
       "<Figure size 640x480 with 10 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    plt.subplot(1, 10, i + 1)\n",
    "    plt.axis('off')\n",
    "    plt.tight_layout()\n",
    "    plt.imshow(samples[i], cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Importing the model in AIDGE ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.120165Z",
     "iopub.status.busy": "2025-02-08T23:24:33.119733Z",
     "iopub.status.idle": "2025-02-08T23:24:33.150061Z",
     "shell.execute_reply": "2025-02-08T23:24:33.148878Z"
    }
   },
   "outputs": [],
   "source": [
    "aidge_model = aidge_onnx.load_onnx(\"ConvNet.onnx\", verbose=False)\n",
    "aidge_core.remove_flatten(aidge_model) # we want to get rid of the 'flatten' nodes ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setting up the AIDGE scheduler ...\n",
    "\n",
    "In order to perform inferences with AIDGE we need to setup a scheduler. But before doing so, we need to create a data producer node and connect it to the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.154628Z",
     "iopub.status.busy": "2025-02-08T23:24:33.154325Z",
     "iopub.status.idle": "2025-02-08T23:24:33.160847Z",
     "shell.execute_reply": "2025-02-08T23:24:33.159675Z"
    }
   },
   "outputs": [],
   "source": [
    "# Insert the input producer\n",
    "input_node = aidge_core.Producer([1, 1, 28, 28], \"XXX\")\n",
    "input_node.add_child(aidge_model)\n",
    "aidge_model.add(input_node)\n",
    "\n",
    "# Set up the backend\n",
    "aidge_model.set_datatype(aidge_core.dtype.float32)\n",
    "aidge_model.set_backend(\"cpu\")\n",
    "\n",
    "# Create the Scheduler\n",
    "scheduler = aidge_core.SequentialScheduler(aidge_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running some example inferences ...\n",
    "\n",
    "Now that the scheduler is ready, let's perform some inferences. To do so we first declare a utility function that will prepare and set our inputs, propagate them and retreive the outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.165467Z",
     "iopub.status.busy": "2025-02-08T23:24:33.165101Z",
     "iopub.status.idle": "2025-02-08T23:24:33.224234Z",
     "shell.execute_reply": "2025-02-08T23:24:33.223113Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " EXAMPLE INFERENCES :\n",
      "7  ->  [[-0.02  0.   -0.03  0.03  0.03  0.01 -0.    0.94  0.02 -0.03]]\n",
      "2  ->  [[ 0.17  0.01  0.8  -0.03  0.01 -0.04  0.06 -0.01 -0.05  0.  ]]\n",
      "1  ->  [[-0.    0.99  0.02  0.02 -0.02 -0.    0.03 -0.01  0.01 -0.04]]\n",
      "0  ->  [[ 0.97  0.03 -0.02  0.01 -0.01  0.04  0.03  0.02  0.01 -0.07]]\n",
      "4  ->  [[-0.05 -0.   -0.01 -0.02  1.13 -0.01 -0.01 -0.03  0.04 -0.01]]\n",
      "1  ->  [[-0.01  1.06 -0.   -0.02 -0.01  0.02 -0.02  0.02  0.01 -0.01]]\n",
      "4  ->  [[-0.03 -0.04  0.01 -0.01  0.94  0.01  0.04  0.03  0.19 -0.12]]\n",
      "9  ->  [[-0.02  0.02  0.07  0.09  0.12 -0.02 -0.02 -0.02  0.12  0.67]]\n",
      "5  ->  [[ 0.03 -0.03  0.04 -0.07  0.01  0.69  0.16  0.06  0.08 -0.02]]\n",
      "9  ->  [[ 0.01 -0.01 -0.   -0.03  0.05 -0.02  0.01  0.06 -0.01  0.95]]\n"
     ]
    }
   ],
   "source": [
    "def propagate(model, scheduler, sample):\n",
    "    # Setup the input\n",
    "    sample = np.reshape(sample, (1, 1, 28, 28))\n",
    "    input_tensor = aidge_core.Tensor(sample)\n",
    "    input_node.get_operator().set_output(0, input_tensor)\n",
    "    # Run the inference\n",
    "    scheduler.forward()\n",
    "    # Gather the results\n",
    "    output_node = model.get_output_nodes().pop()\n",
    "    output_tensor = output_node.get_operator().get_output(0)\n",
    "    return np.array(output_tensor)\n",
    "\n",
    "print('\\n EXAMPLE INFERENCES :')\n",
    "for i in range(10):\n",
    "    output_array = propagate(aidge_model, scheduler, samples[i])\n",
    "    print(labels[i] , ' -> ', np.round(output_array, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Computing the model accuracy ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.228741Z",
     "iopub.status.busy": "2025-02-08T23:24:33.228450Z",
     "iopub.status.idle": "2025-02-08T23:24:33.664095Z",
     "shell.execute_reply": "2025-02-08T23:24:33.662835Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " MODEL ACCURACY : 100.000%\n"
     ]
    }
   ],
   "source": [
    "def compute_accuracy(model, samples, labels):\n",
    "    acc = 0\n",
    "    for i, x in enumerate(samples):\n",
    "        y = propagate(model, scheduler, x)\n",
    "        if labels[i] == np.argmax(y):\n",
    "            acc += 1\n",
    "    return acc / len(samples)\n",
    "\n",
    "accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)\n",
    "print(f'\\n MODEL ACCURACY : {accuracy * 100:.3f}%')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Quantization dataset creation ...\n",
    "\n",
    "We need to convert a subset of our Numpy samples into AIDGE tensors, so that they can be used to compute the activation ranges."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.668810Z",
     "iopub.status.busy": "2025-02-08T23:24:33.668485Z",
     "iopub.status.idle": "2025-02-08T23:24:33.677408Z",
     "shell.execute_reply": "2025-02-08T23:24:33.676253Z"
    }
   },
   "outputs": [],
   "source": [
    "tensors = []\n",
    "for sample in samples[0:NB_SAMPLES]:\n",
    "    sample = np.reshape(sample, (1, 1, 28, 28))\n",
    "    tensor = aidge_core.Tensor(sample)\n",
    "    tensors.append(tensor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Applying the PTQ to the model ...\n",
    "\n",
    "Now that everything is ready, we can call the PTQ routine ! Note that after the quantization we need to update the scheduler."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:33.681910Z",
     "iopub.status.busy": "2025-02-08T23:24:33.681566Z",
     "iopub.status.idle": "2025-02-08T23:24:34.082602Z",
     "shell.execute_reply": "2025-02-08T23:24:34.081337Z"
    }
   },
   "outputs": [],
   "source": [
    "aidge_quantization.quantize_network(aidge_model, NB_BITS, tensors)\n",
    "\n",
    "scheduler = aidge_core.SequentialScheduler(aidge_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running some quantized inferences ...\n",
    "\n",
    "Now that our network is quantized, what about testing some inferences ? Let's do so, but before, we need not to forget that our 8-bit network expect 8-bit inputs ! We thus need to rescale the input tensors ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:34.087194Z",
     "iopub.status.busy": "2025-02-08T23:24:34.086871Z",
     "iopub.status.idle": "2025-02-08T23:24:34.094526Z",
     "shell.execute_reply": "2025-02-08T23:24:34.093425Z"
    }
   },
   "outputs": [],
   "source": [
    "scaling = 2**(NB_BITS-1)-1\n",
    "for i in range(NB_SAMPLES):\n",
    "    samples[i] = np.round(samples[i] * scaling)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now perform our quantized inferences ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:34.100622Z",
     "iopub.status.busy": "2025-02-08T23:24:34.100298Z",
     "iopub.status.idle": "2025-02-08T23:24:34.160836Z",
     "shell.execute_reply": "2025-02-08T23:24:34.159457Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " EXAMPLE QUANTIZED INFERENCES :\n",
      "7  ->  [[ -1.86   0.5   -3.07   3.28   2.93   1.1   -0.12 103.51   1.85  -2.92]]\n",
      "2  ->  [[ 1.872e+01  1.250e+00  8.744e+01 -3.360e+00  6.600e-01 -4.480e+00\n",
      "   6.280e+00 -1.130e+00 -6.000e+00  6.000e-02]]\n",
      "1  ->  [[ -0.15 109.     2.27   1.93  -1.75  -0.39   3.53  -1.16   1.36  -4.48]]\n",
      "0  ->  [[106.18   3.42  -2.7    0.72  -0.62   4.9    2.75   1.91   0.92  -7.82]]\n",
      "4  ->  [[-5.7100e+00 -9.0000e-02 -9.7000e-01 -1.9100e+00  1.2401e+02 -9.6000e-01\n",
      "  -1.3900e+00 -2.9600e+00  4.3000e+00 -1.0500e+00]]\n",
      "1  ->  [[ -0.57 116.28  -0.4   -2.17  -1.38   1.62  -2.5    1.66   1.49  -1.27]]\n",
      "4  ->  [[ -3.32  -4.14   0.61  -1.43 102.52   1.56   4.78   3.59  20.92 -12.72]]\n",
      "9  ->  [[-2.31  2.32  7.17  9.86 12.81 -2.33 -2.67 -1.89 13.38 73.47]]\n",
      "5  ->  [[ 3.16 -3.83  4.07 -8.06  1.2  76.01 18.02  6.82  8.64 -2.14]]\n",
      "9  ->  [[ 1.2800e+00 -6.7000e-01 -8.0000e-02 -3.4100e+00  4.9700e+00 -1.7700e+00\n",
      "   1.4800e+00  6.6500e+00 -1.4700e+00  1.0455e+02]]\n"
     ]
    }
   ],
   "source": [
    "print('\\n EXAMPLE QUANTIZED INFERENCES :')\n",
    "for i in range(10):\n",
    "    input_array = np.reshape(samples[i], (1, 1, 28, 28))\n",
    "    output_array = propagate(aidge_model, scheduler, input_array)\n",
    "    print(labels[i] , ' -> ', np.round(output_array, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing the quantized accuracy ...\n",
    "\n",
    "Just as we've done for the initial network, we can compute the quantized model accuracy ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:34.166480Z",
     "iopub.status.busy": "2025-02-08T23:24:34.165956Z",
     "iopub.status.idle": "2025-02-08T23:24:34.575600Z",
     "shell.execute_reply": "2025-02-08T23:24:34.574725Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " QUANTIZED MODEL ACCURACY : 100.000%\n"
     ]
    }
   ],
   "source": [
    "accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)\n",
    "print(f'\\n QUANTIZED MODEL ACCURACY : {accuracy * 100:.3f}%')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Work is done !\n",
    "\n",
    "We see that a 8-bit PTQ does not affect the accuracy of our model ! This result shows that a proper quantization algorithm can be used to deploy a Neural Network on very small devices, where manipulating bytes is optimal. We encourage you to run this notebook again with even more aggressive quantization values !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2025-02-08T23:24:34.580068Z",
     "iopub.status.busy": "2025-02-08T23:24:34.579768Z",
     "iopub.status.idle": "2025-02-08T23:24:34.586498Z",
     "shell.execute_reply": "2025-02-08T23:24:34.585051Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "That's all folks !\n"
     ]
    }
   ],
   "source": [
    "print('That\\'s all folks !')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_aidge2",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
